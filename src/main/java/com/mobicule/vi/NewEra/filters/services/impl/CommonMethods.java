package com.mobicule.vi.NewEra.filters.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import com.mobicule.vi.NewEra.exceptions.PropertyFileNotFoundException;
import com.mobicule.vi.NewEra.exceptions.PropertyFilePathNotFoundException;
import com.mobicule.vi.NewEra.exceptions.ValueFromPropertyFileNotFoundException;

@Component
public class CommonMethods {

	ClassLoader classloader = Thread.currentThread().getContextClassLoader();

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private Environment env;

	public String getPropertyFilePath(String propertyFileName) throws PropertyFilePathNotFoundException {
		InputStream inputStream;
		String propertyFilePath = null;
		inputStream = classloader.getResourceAsStream(env.getProperty("propertyFiles"));
		Map<String, String> data = new Yaml().load(inputStream);
		log.info("Property Files Paths : " + data);
		propertyFilePath = data.get(propertyFileName);
		log.info("Property File Path : " + propertyFilePath);
		if (propertyFilePath == null)
			throw new PropertyFilePathNotFoundException(propertyFileName);
		return propertyFilePath;
	}

	public String getValueFromPropertyFile(String propertyFilePath, String key)
			throws ValueFromPropertyFileNotFoundException, IOException {
		String value = null;
		FileInputStream fileInput = null;
		Properties properties = null;
		try {
			fileInput = new FileInputStream(new File(propertyFilePath));
			properties = new Properties();
			properties.load(fileInput);
			fileInput.close();
			value = properties.getProperty(key);
		} catch (IOException e) {
			if (fileInput != null)
				fileInput.close();
		}
		if (properties == null)
			throw new PropertyFileNotFoundException(propertyFilePath);
		if (value == null)
			throw new ValueFromPropertyFileNotFoundException(propertyFilePath, key);
		return value;
	}

	public Map<String, String> getAppSettings() {
		InputStream inputStream = classloader.getResourceAsStream(env.getProperty("appSetting"));
		return new Yaml().load(inputStream);
	}
}
