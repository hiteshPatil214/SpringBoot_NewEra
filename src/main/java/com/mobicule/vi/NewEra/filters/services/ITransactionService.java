package com.mobicule.vi.NewEra.filters.services;

import com.mobicule.vi.NewEra.entities.Request;
import com.mobicule.vi.NewEra.entities.Response;

public interface ITransactionService {

	Response processRequest(Request request) throws Exception;
}
