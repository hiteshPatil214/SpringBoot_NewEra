package com.mobicule.vi.NewEra.entities;

public class User {
	private String syncCheck;
	private String _long;
	private String versionCode;
	private String network;
	private String client;
	private String etop;
	private String entityId;
	private String deviceModel;
	private String version;
	private String syncDate;
	private String lat;
	private String checkVersion;
	private String opt2;
	private String opt1;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(String syncCheck, String _long, String versionCode, String network, String client, String etop,
			String entityId, String deviceModel, String version, String syncDate, String lat, String checkVersion,
			String opt2, String opt1) {
		super();
		this.syncCheck = syncCheck;
		this._long = _long;
		this.versionCode = versionCode;
		this.network = network;
		this.client = client;
		this.etop = etop;
		this.entityId = entityId;
		this.deviceModel = deviceModel;
		this.version = version;
		this.syncDate = syncDate;
		this.lat = lat;
		this.checkVersion = checkVersion;
		this.opt2 = opt2;
		this.opt1 = opt1;
	}

	public String getSyncCheck() {
		return syncCheck;
	}

	public void setSyncCheck(String syncCheck) {
		this.syncCheck = syncCheck;
	}

	public String get_long() {
		return _long;
	}

	public void set_long(String _long) {
		this._long = _long;
	}

	public String getVersionCode() {
		return versionCode;
	}

	public void setVersionCode(String versionCode) {
		this.versionCode = versionCode;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getEtop() {
		return etop;
	}

	public void setEtop(String etop) {
		this.etop = etop;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getSyncDate() {
		return syncDate;
	}

	public void setSyncDate(String syncDate) {
		this.syncDate = syncDate;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getCheckVersion() {
		return checkVersion;
	}

	public void setCheckVersion(String checkVersion) {
		this.checkVersion = checkVersion;
	}

	public String getOpt2() {
		return opt2;
	}

	public void setOpt2(String opt2) {
		this.opt2 = opt2;
	}

	public String getOpt1() {
		return opt1;
	}

	public void setOpt1(String opt1) {
		this.opt1 = opt1;
	}

	@Override
	public String toString() {
		return "User [syncCheck=" + syncCheck + ", _long=" + _long + ", versionCode=" + versionCode + ", network="
				+ network + ", client=" + client + ", etop=" + etop + ", entityId=" + entityId + ", deviceModel="
				+ deviceModel + ", version=" + version + ", syncDate=" + syncDate + ", lat=" + lat + ", checkVersion="
				+ checkVersion + ", opt2=" + opt2 + ", opt1=" + opt1 + "]";
	}

}
