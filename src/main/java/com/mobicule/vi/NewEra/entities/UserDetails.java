package com.mobicule.vi.NewEra.entities;

import java.sql.Timestamp;

public class UserDetails {
	private Long id;
	private String mobNo;
	private Long circleCode;
	private String otp;
	private String entityCode;
	private Timestamp createdOn;
	private Long createdBy;
	private Timestamp modifiedOn;
	private Long modifiedBy;
	private String deleteFlag;
	private String loginAs;
	private String msgId;
	private String session_token;
	private Timestamp token_createdOn;
	private Timestamp user_login_time;
	private Timestamp user_logout_time;

	public UserDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserDetails(Long id, String mobNo, Long circleCode, String otp, String entityCode, Timestamp createdOn,
			Long createdBy, Timestamp modifiedOn, Long modifiedBy, String deleteFlag, String loginAs, String msgId,
			String session_token, Timestamp token_createdOn, Timestamp user_login_time, Timestamp user_logout_time) {
		super();
		this.id = id;
		this.mobNo = mobNo;
		this.circleCode = circleCode;
		this.otp = otp;
		this.entityCode = entityCode;
		this.createdOn = createdOn;
		this.createdBy = createdBy;
		this.modifiedOn = modifiedOn;
		this.modifiedBy = modifiedBy;
		this.deleteFlag = deleteFlag;
		this.loginAs = loginAs;
		this.msgId = msgId;
		this.session_token = session_token;
		this.token_createdOn = token_createdOn;
		this.user_login_time = user_login_time;
		this.user_logout_time = user_logout_time;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobNo() {
		return mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public Long getCircleCode() {
		return circleCode;
	}

	public void setCircleCode(Long circleCode) {
		this.circleCode = circleCode;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getEntityCode() {
		return entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public Long getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(Long modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public String getLoginAs() {
		return loginAs;
	}

	public void setLoginAs(String loginAs) {
		this.loginAs = loginAs;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}

	public String getSession_token() {
		return session_token;
	}

	public void setSession_token(String session_token) {
		this.session_token = session_token;
	}

	public Timestamp getToken_createdOn() {
		return token_createdOn;
	}

	public void setToken_createdOn(Timestamp token_createdOn) {
		this.token_createdOn = token_createdOn;
	}

	public Timestamp getUser_login_time() {
		return user_login_time;
	}

	public void setUser_login_time(Timestamp user_login_time) {
		this.user_login_time = user_login_time;
	}

	public Timestamp getUser_logout_time() {
		return user_logout_time;
	}

	public void setUser_logout_time(Timestamp user_logout_time) {
		this.user_logout_time = user_logout_time;
	}

	@Override
	public String toString() {
		return "UserDetails [id=" + id + ", mobNo=" + mobNo + ", circleCode=" + circleCode + ", otp=" + otp
				+ ", entityCode=" + entityCode + ", createdOn=" + createdOn + ", createdBy=" + createdBy
				+ ", modifiedOn=" + modifiedOn + ", modifiedBy=" + modifiedBy + ", deleteFlag=" + deleteFlag
				+ ", loginAs=" + loginAs + ", msgId=" + msgId + ", session_token=" + session_token
				+ ", token_createdOn=" + token_createdOn + ", user_login_time=" + user_login_time
				+ ", user_logout_time=" + user_logout_time + "]";
	}

}
