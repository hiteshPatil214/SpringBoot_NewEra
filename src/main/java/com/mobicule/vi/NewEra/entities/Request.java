package com.mobicule.vi.NewEra.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Request {

	private User user;
	private String entity;
	private String type;
	private Map<String, Object> queryParameterMap = new HashMap<>();
	private List<Object> data = new ArrayList<>();
	private String action;

	public Request() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Request(User user, String entity, String type, Map<String, Object> queryParameterMap, List<Object> data,
			String action) {
		super();
		this.user = user;
		this.entity = entity;
		this.type = type;
		this.queryParameterMap = queryParameterMap;
		this.data = data;
		this.action = action;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getEntity() {
		return entity;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, Object> getQueryParameterMap() {
		return queryParameterMap;
	}

	public void setQueryParameterMap(Map<String, Object> queryParameterMap) {
		this.queryParameterMap = queryParameterMap;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return "Request [user=" + user + ", entity=" + entity + ", type=" + type + ", queryParameterMap="
				+ queryParameterMap + ", data=" + data + ", action=" + action + "]";
	}
}
