package com.mobicule.vi.NewEra.exceptions;

import java.util.ArrayList;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.mobicule.vi.NewEra.entities.Response;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(HandlerBeanNotFoundException.class)
	public ResponseEntity<Response> handlerBeanNotFoundExceptionHandler(HandlerBeanNotFoundException exception) {
		return new ResponseEntity<Response>(new Response(exception.getMessage(), "FAILURE", new ArrayList<>()),
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(PropertyFilePathNotFoundException.class)
	public ResponseEntity<Response> propertyFilePathNotFoundExceptionHandler(
			PropertyFilePathNotFoundException exception) {
		return new ResponseEntity<Response>(new Response(exception.getMessage(), "FAILURE", new ArrayList<>()),
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(HandlerNotFoundException.class)
	public ResponseEntity<Response> handlerNotFoundExceptionHandler(HandlerNotFoundException exception) {
		return new ResponseEntity<Response>(new Response(exception.getMessage(), "FAILURE", new ArrayList<>()),
				HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(PropertyFileNotFoundException.class)
	public ResponseEntity<Response> propertyFileNotFoundExceptionHandler(PropertyFileNotFoundException exception) {
		return new ResponseEntity<Response>(new Response(exception.getMessage(), "FAILURE", new ArrayList<>()),
				HttpStatus.NOT_FOUND);
	}
}
