package com.mobicule.vi.NewEra.exceptions;

import java.io.FileNotFoundException;

public class PropertyFileNotFoundException extends FileNotFoundException {

	private static final long serialVersionUID = 1L;

	public PropertyFileNotFoundException(String propertyFileName) {
		super(String.format("Property file not found '%s'", propertyFileName));
	}
}
