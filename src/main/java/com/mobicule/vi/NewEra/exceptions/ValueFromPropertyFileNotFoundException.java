package com.mobicule.vi.NewEra.exceptions;

import java.io.FileNotFoundException;

public class ValueFromPropertyFileNotFoundException extends FileNotFoundException {

	private static final long serialVersionUID = 1L;

	public ValueFromPropertyFileNotFoundException(String propertyFilePath, String key) {
		super(String.format("Value against '%s' is not found in the '%s' property file", key, propertyFilePath));
	}
}
