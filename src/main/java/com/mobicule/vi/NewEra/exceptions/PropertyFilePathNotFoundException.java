package com.mobicule.vi.NewEra.exceptions;

import java.io.FileNotFoundException;

public class PropertyFilePathNotFoundException extends FileNotFoundException {

	private static final long serialVersionUID = 1L;

	public PropertyFilePathNotFoundException(String propertyFileName) {
		super(String.format("Property file path not found for file '%s'", propertyFileName));
	}
}
